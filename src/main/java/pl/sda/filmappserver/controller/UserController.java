package pl.sda.filmappserver.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.sda.filmappmodel.model.User;
import pl.sda.filmappserver.manager.UserManager;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger LOG = Logger.getLogger(UserController.class);

    @Autowired
    private UserManager userManager;

    @RequestMapping(value = "/signup",
            method = RequestMethod.POST,
            consumes = "application/json")
    public void createUser(@RequestBody User user) {
        userManager.save(user);
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<User> getAllUsers() {
        LOG.debug("get all users from db");
        return userManager.findAll();
    }

    @RequestMapping(value = "/id", method = RequestMethod.GET)
    @ResponseBody
    public User getUserById(@PathVariable("id") int id) {
        return userManager.findById(id);
    }

    @RequestMapping(value = "/username", method = RequestMethod.GET)
    @ResponseBody
    public User getUserByUsername(@PathVariable("username") String username) {
        return userManager.findByUsername(username);
    }

    @RequestMapping(value = "/id", method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable("id") int id) {
        userManager.deleteById(id);
    }

    @RequestMapping(value = "/username", method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable("username") String username) {
        userManager.deleteByUsername(username);
    }
}
