package pl.sda.filmappserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.sda.filmappmodel.model.Film;
import pl.sda.filmappserver.manager.FilmManager;

import java.util.List;

@RestController
@RequestMapping("/film")
public class FilmController {

    @Autowired
    private FilmManager filmMenager;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public void addFilm(Film film) {
        filmMenager.save(film);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public List<Film> getAllFilms() {
        return filmMenager.findAll();
    }

    @RequestMapping(value = "/id", method = RequestMethod.GET)
    @ResponseBody
    public Film getFilmById(@PathVariable("id") int id) {
        return filmMenager.findById(id);
    }

    @RequestMapping(value = "/director", method = RequestMethod.GET)
    @ResponseBody
    public List<Film> getFilmByDirector(@PathVariable("director") String director) {
        return filmMenager.findByDirector(director);
    }

    @RequestMapping(value = "/title", method = RequestMethod.GET)
    @ResponseBody
    public List<Film> getFilmByTitle(@PathVariable("title") String title) {
        return filmMenager.findByTitle(title);
    }

    @RequestMapping(value = "/year", method = RequestMethod.GET)
    @ResponseBody
    public List<Film> getFilmByYear(@PathVariable("year") int year) {
        return filmMenager.findByYear(year);
    }

    @RequestMapping(value = "/id", method = RequestMethod.DELETE)
    public void deleteFilm(@PathVariable("id") int id) {
        filmMenager.deleteById(id);
    }

}