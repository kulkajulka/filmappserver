package pl.sda.filmappserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.sda.filmappmodel.model.Actor;
import pl.sda.filmappserver.manager.ActorManager;

import java.util.List;


@RestController
@RequestMapping("/actor")
public class ActorController {

    @Autowired
    private ActorManager actorManager;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public void addActor(Actor actor) {
        actorManager.save(actor);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public List<Actor> getAllActors() {
        return actorManager.findAll();
    }

    @RequestMapping(value = "/id", method = RequestMethod.GET)
    @ResponseBody
    public Actor getActorById(@PathVariable("id") int id) {
        return actorManager.findById(id);
    }

    @RequestMapping(value = "/names", method = RequestMethod.GET)
    @ResponseBody
    public List<Actor> getActorByFirstnameOrLastname(@PathVariable("names") String firstname, String lastname) {
        return actorManager.findByFirstNameOrLastName(firstname, lastname);
    }

    @RequestMapping(value = "/id", method = RequestMethod.DELETE)
    public void deleteActor(@PathVariable("id") int id) {
        actorManager.deleteById(id);
    }


}

