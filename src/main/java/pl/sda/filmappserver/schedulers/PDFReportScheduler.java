package pl.sda.filmappserver.schedulers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.sda.filmappmodel.model.Film;
import pl.sda.filmappmodel.model.User;
import pl.sda.filmappserver.manager.FilmManager;
import pl.sda.filmappserver.manager.UserManager;
import pl.sda.filmappserver.pdfreports.PdfGenerator;

import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class PDFReportScheduler {
    private static AtomicLong FILM_PDF_COUNTER = new AtomicLong(0);
    private static AtomicLong USER_PDF_COUNTER = new AtomicLong(0);

    @Autowired
    FilmManager filmManager;

    @Autowired
    PdfGenerator generator;

    @Autowired
    UserManager userManager;

    @Scheduled(cron = "${cron.config.films}")
    public void generateTop10FilmsPDFReport() {
        List<Film> filmList = filmManager.findTopTenByImdbRating();
        String fileName = new StringBuilder(new Date().toString())
                .append(" TOP 10 FILMS (")
                .append(FILM_PDF_COUNTER.incrementAndGet())
                .append(")")
                .toString();
        generator.generate(fileName,filmList,"TOP 10 MOVIES EVER");
    }

    @Scheduled(cron = "${cron.config.users}")
    public void generateTop5ActiveUsersPDFReport() {
        List<User> userList = userManager.findTop5BySignIn();
        String fileName = new StringBuilder(new Date().toString())
                .append(" TOP 5 USERS (")
                .append(USER_PDF_COUNTER.incrementAndGet())
                .append(")")
                .toString();
        generator.generate(fileName,userList,"TOP 5 ACTIVE USERS");
    }
}
