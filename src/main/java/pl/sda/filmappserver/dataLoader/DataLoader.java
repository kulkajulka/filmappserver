package pl.sda.filmappserver.dataLoader;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.sda.filmappmodel.model.*;
import pl.sda.filmappmodel.parser.MoviesJsonParser;
import pl.sda.filmappserver.manager.FilmManager;
import pl.sda.filmappserver.manager.GenreManager;
import pl.sda.filmappserver.manager.UserManager;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DataLoader {

    private static final Logger log = Logger.getLogger(DataLoader.class);

    @Autowired
    FilmManager filmManager;

    @Autowired
    UserManager userManager;

    @Autowired
    GenreManager genreManager;

    @Value("${json.datafile}")
    private String jsonFile;

    @Value("${admin.username}")
    private String adminUsername;

    @Value("${admin.password}")
    private String adminPassword;

    @Value("${admin.firstname}")
    private String adminFirstName;

    @Value("${admin.lastname}")
    private String adminLastName;

    @Value("${admin.genre}")
    private String genrePreference;

    @Value("${admin.year}")
    private int adminYear;

    @Value("${admin.month}")
    private int adminMonth;

    @Value("${admin.day}")
    private int adminDay;

    @Value("${user.username}")
    private String userUsername;

    @Value("${user.password}")
    private String userPassword;

    @Value("${user.firstname}")
    private String userFirstName;

    @Value("${user.lastname}")
    private String userLastName;

    @Value("${user.genre}")
    private String userGenrePreference;

    @Value("${user.year}")
    private int userYear;

    @Value("${user.month}")
    private int userMonth;

    @Value("${user.day}")
    private int userDay;

    @PostConstruct
    public void loadData(){
        List<Film> films = MoviesJsonParser.parseJsonsFromFile(jsonFile);
        HashSet<Genre> genres = films.stream().map(Film::getGenre)
                .collect(Collectors.toCollection(HashSet::new));
        genres.forEach(genre -> {
            if (genre != null) genreManager.save(genre);
        });
        log.info("Genres saved!");
        log.info("inserting data to DB:"+films.size()+ "-objects are inserted to DB");
        filmManager.saveAll(films);
        User admin = new User(adminUsername,
                adminPassword,
                adminFirstName,
                adminLastName,
                genrePreference,
                Date.from(LocalDate.of(adminYear,adminMonth,adminDay).atStartOfDay(ZoneId.systemDefault()).toInstant()),
                UserRole.ADMIN);
        userManager.save(admin);
        User user = new User(userUsername,
                userPassword,
                userFirstName,
                userLastName,
                userGenrePreference,
                Date.from(LocalDate.of(userYear,userMonth,userDay).atStartOfDay(ZoneId.systemDefault()).toInstant()),
                UserRole.USER);
        userManager.save(user);
    }
}
