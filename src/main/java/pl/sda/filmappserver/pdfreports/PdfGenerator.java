package pl.sda.filmappserver.pdfreports;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.sda.filmappmodel.model.Film;
import pl.sda.filmappmodel.model.User;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import static org.apache.log4j.Logger.*;

@Component
public class PdfGenerator {
    private static final Logger LOGGER = getLogger(PdfGenerator.class);
    private static final int FONT_SIZE = 16;

    @Value("${pdf.destination}")
    private String RESOURCES_PATH;

    private String buildFileName(String name) {
        return new StringBuilder()
                .append(RESOURCES_PATH)
                .append(name)
                .append(".pdf")
                .toString();
    }

    public String getRESOURCES_PATH() {
        return RESOURCES_PATH;
    }

    private File buildPdfFile(String fileName){
        return new File(buildFileName(fileName));
    }

    private FileOutputStream fileOutputStream (String pdfFile) throws FileNotFoundException {
        return new FileOutputStream(buildPdfFile(pdfFile));
    }

    private Paragraph title(String title){
        return new Paragraph(title, font());
    }

    private Font font (){
        return FontFactory.getFont(FontFactory.COURIER, FONT_SIZE , BaseColor.RED);
    }

    private Paragraph space(){
        return new Paragraph("*********************", font());
    }

    public com.itextpdf.text.List toItextList(List list) {
        com.itextpdf.text.List iTextlist = new com.itextpdf.text.List();
        for (Object elem: list) {
            if (elem instanceof  Film){
                films(iTextlist, (Film) elem);
            }
            if (elem instanceof User){
                users(iTextlist, (User) elem);
            }
        }
        return iTextlist;
    }

    private void users(com.itextpdf.text.List iTextlist, User elem) {
        iTextlist.add(new StringBuilder()
                .append(elem.getFirstName())
                .append(" ")
                .append(elem.getLastName())
                .append(" loggin count:")
                .append(elem.getSignInCounter())
                .toString());
    }

    private void films(com.itextpdf.text.List iTextlist, Film elem) {
        iTextlist.add(new StringBuilder()
                .append(elem.getTitle())
                .append(" ")
                .append(elem.getImdb().getRating())
                .toString());
    }

    public void generate (String fileName, List list, String title) {
        Document pdf = new Document();
        try {
            PdfWriter.getInstance(pdf,fileOutputStream(fileName));
            pdf.open();
            pdf.add(title(title));
            pdf.add(space());
            pdf.add(toItextList(list));
            pdf.close();
        } catch (DocumentException e) {
            LOGGER.error(e.getMessage());
        } catch (FileNotFoundException e) {
            LOGGER.error(e.getMessage());
        }
    }
}
