package pl.sda.filmappserver;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import pl.sda.filmappmodel.model.User;
import pl.sda.filmappmodel.model.UserRole;
import pl.sda.filmappmodel.util.EncryptedPair;
import pl.sda.filmappserver.manager.UserManager;

import java.util.Date;

import static pl.sda.filmappmodel.util.PasswordEncryptor.encryptPassword;

@EntityScan("pl.sda.filmappmodel.*")
@SpringBootApplication(scanBasePackages = "pl.sda.*")
@EnableScheduling
public class FilmappServerApplication implements CommandLineRunner {

    private static final Logger logger = Logger.getLogger(FilmappServerApplication.class);

    @Autowired
    UserManager userManager;

    public static void main(String[] args) {
        SpringApplication.run(FilmappServerApplication.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        User user = new User();
        user.setBirthDate(new Date());
        user.setGenrePreference("Western");
        user.setFirstName("Marcin");
        user.setLastName("Kowalski");
        user.setUsername("User");
        EncryptedPair pair = encryptPassword("1234512345123451234512345");
        user.setHash(pair.getHash());
        user.setSalt(pair.getSalt());
        user.setSignInCounter(1L);
        user.setRole(UserRole.USER);
        logger.debug("Before saving user:" + user);
        userManager.save(user);
        logger.debug("User saved!" + user);
    }
}
