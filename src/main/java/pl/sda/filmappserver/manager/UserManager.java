package pl.sda.filmappserver.manager;

import org.apache.commons.collections.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.sda.filmappmodel.model.User;
import pl.sda.filmappserver.repository.UserRepository;

import java.util.List;

@Component
public class UserManager {

    @Autowired
    UserRepository userRepository;

    public void save(User user) {
        userRepository.save(user);
    }

    public void deleteByUsername(String username) {
        userRepository.deleteByUsername(username);
    }

    public void deleteById(int id) {
        userRepository.deleteById(id);
    }

    public User findById(int id) {
        return userRepository.findOne(id);
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public List<User> findAll() {
        return IteratorUtils.toList(userRepository.findAll().iterator());
    }

    public List<User> findTop5BySignIn() {
        return userRepository.findTop5ByOrderBySignInCounterDesc();
    }
}
