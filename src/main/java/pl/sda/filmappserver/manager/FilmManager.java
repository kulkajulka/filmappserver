package pl.sda.filmappserver.manager;

import org.apache.commons.collections.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.sda.filmappmodel.model.Film;
import pl.sda.filmappserver.repository.FilmRepository;

import java.util.List;

@Component
public class FilmManager {

    @Autowired
    FilmRepository filmRepository;

    public void save(Film film) {
        filmRepository.save(film);
    }

    public void deleteById(int id) {
        filmRepository.delete(id);
    }

    public Film findById(int id) {
        return filmRepository.findById(id);
    }

    public List<Film> findByTitle(String title) {
        return filmRepository.findByTitle(title);
    }

    public List<Film> findByDirector(String director) {
        return filmRepository.findByDirector(director);
    }

    public List<Film> findByYear(int year) {
        return filmRepository.findByYear(year);
    }

    public List<Film> findAll() {
        return IteratorUtils.toList(filmRepository.findAll().iterator());
    }

    public List<Film> findTopTenByImdbRating() {
        return filmRepository.findTop10ByOrderByImdbRatingDesc();
    }

    public Iterable<Film> saveAll(List<Film> films) {
        return filmRepository.save(films);
    }
}


