package pl.sda.filmappserver.manager;

import org.apache.commons.collections.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import pl.sda.filmappmodel.model.Genre;
import pl.sda.filmappserver.repository.GenreRepository;

import java.util.List;

@Component
public class GenreManager {

    @Autowired
    GenreRepository genreRepository;

    public void save(Genre genre) {
        genreRepository.save(genre);
    }

    public Genre findByName(String name) {
        return genreRepository.findByName(name);
    }

    public List<Genre> findAll(){
        return IteratorUtils.toList(genreRepository.findAll().iterator());
    }
}
