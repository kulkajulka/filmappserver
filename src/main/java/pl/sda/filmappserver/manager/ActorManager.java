package pl.sda.filmappserver.manager;

import org.apache.commons.collections.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.sda.filmappmodel.model.Actor;
import pl.sda.filmappmodel.model.Film;
import pl.sda.filmappserver.repository.ActorRepository;

import java.util.List;

@Component
public class ActorManager {

    @Autowired
    ActorRepository actorRepository;

    public void save(Actor actor) {
        actorRepository.save(actor);
    }

    public Actor findById(int id) {
        return actorRepository.findById(id);
    }

    public List<Actor> findByFirstNameOrLastName(String firstName, String lastName) {
        return actorRepository.findByFirstNameOrLastName(firstName, lastName);
    }

    public Actor findByLastName(String lastName){
        return actorRepository.findByLastName(lastName);
    }

    public List<Actor> findAll() {
        return IteratorUtils.toList(actorRepository.findAll().iterator());
    }

    public void deleteById(int id) {
        actorRepository.deleteById(id);
    }

    public void deleteByLastName(String lastName){
        actorRepository.deleteByLastName(lastName);
    }
}


