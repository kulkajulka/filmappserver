package pl.sda.filmappserver.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = {"classpath:/application.properties",
        "file:${user.home}/conf.properties"},
        ignoreResourceNotFound = true)
public class ConfigProperties {

}