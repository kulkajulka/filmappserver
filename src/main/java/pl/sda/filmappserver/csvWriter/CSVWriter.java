package pl.sda.filmappserver.csvWriter;

import org.apache.commons.csv.CSVPrinter;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Field;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.lang.System.currentTimeMillis;
import static org.apache.commons.csv.CSVFormat.RFC4180;
import static org.apache.log4j.Logger.getLogger;

public class CSVWriter {

    private final static Logger LOGGER = getLogger(CSVWriter.class);

    private static final String DIRECTORY = "csv-films";

    public static <T> File saveToCSVFile(List<T> objectList) throws IllegalAccessException {
        if (createDirectoryIfNotExist(DIRECTORY)) {
            File csv = Paths.get(System.getProperty("user.home"), DIRECTORY, getTimeStamp()).toFile();
            LOGGER.info("created filePath : " + csv);

            try {
                Writer writer = new FileWriter(csv);
                LOGGER.info("created file : " + writer);
                saveToFile(writer, objectList);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return csv;
        } else {
            return null;
        }

    }

    private static boolean createDirectoryIfNotExist(String dirName) {
        File theDir = Paths.get(System.getProperty("user.home"), dirName).toFile();

        if (!theDir.exists()) {
            LOGGER.info("creating directory: " + theDir.getName());

            try {
                LOGGER.info("DIR created");
                return theDir.mkdir();
            } catch (SecurityException e) {
                e.printStackTrace();
                LOGGER.error("Throw SecurityException");
            }
        }
        return false;
    }

    private static <T> void saveToFile(Writer writer, List<T> objectList) throws IOException, IllegalAccessException {
        List<String> valueList = new ArrayList<>();
        String[] headersNames = setHeaders(objectList);
        try (
                CSVPrinter printer = RFC4180
                        .withHeader(headersNames)
                        .print(writer)) {

            for (T anObjectList : objectList) {
                Field[] fields = anObjectList.getClass().getDeclaredFields();
                addFirstRowFieldsToFile(valueList, anObjectList, fields);
                printer.printRecord(valueList);
                LOGGER.debug("print record : " + valueList);
                printer.flush();
                LOGGER.debug("CSVPrinter flush");
                valueList.clear();
                LOGGER.info("valueList clear");
            }
            writer.flush();
            LOGGER.debug("writer flush");
            writer.close();
            LOGGER.debug("writer close");
            printer.close();
            LOGGER.debug("CSVPrinter close");
        }
    }

    private static <T> void addFirstRowFieldsToFile(List<String> valueList, T anObjectList, Field[] fields) throws IllegalAccessException {
        for (Field singleField : fields) {
            singleField.setAccessible(true);
            //TODO pass through collection
            if (!Collection.class.isAssignableFrom(singleField.getType()) && singleField.get(anObjectList) != null) {
                LOGGER.debug("field name : " + singleField.getName() + " type - " + singleField.getType());
                LOGGER.debug("add " + singleField.getName() + " value : " + singleField.get(anObjectList));
                valueList.add(singleField.get(anObjectList).toString());
            }
        }
    }

    private static <T> String[] setHeaders(List<T> objectList) {

        Field[] headers = objectList.get(0).getClass().getDeclaredFields();
        List<String> headersNamesList = new ArrayList<>();

        for (int i = 0; i < headers.length; i++) {
            Field[] tableHeaders = objectList.get(0).getClass().getDeclaredFields();
            //remove if below if all headers is needed
            if (!Collection.class.isAssignableFrom(tableHeaders[i].getType())) {
                LOGGER.debug("header name : " + tableHeaders[i].getName());
                headersNamesList.add(tableHeaders[i].getName());
            }
        }

        //TODO refactor
        String[] headersNamesTable = new String[headersNamesList.size()];
        for (int i = 0; i < headersNamesList.size(); i++) {
            headersNamesTable[i] = headersNamesList.get(i);
        }
        return headersNamesTable;
    }

    private static String getTimeStamp() {
        return new Timestamp(currentTimeMillis()).toString().trim() + ".csv";
    }

}
