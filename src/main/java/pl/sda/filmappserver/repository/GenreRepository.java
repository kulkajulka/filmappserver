package pl.sda.filmappserver.repository;

import org.springframework.data.repository.CrudRepository;
import pl.sda.filmappmodel.model.Genre;

import java.util.List;

public interface GenreRepository extends CrudRepository<Genre, Integer> {

    Genre findByName(String name);

    List<Genre> findById(int id);
}
