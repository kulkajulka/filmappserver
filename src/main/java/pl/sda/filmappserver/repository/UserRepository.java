package pl.sda.filmappserver.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import pl.sda.filmappmodel.model.User;

import java.sql.Date;
import java.util.List;

public interface UserRepository extends CrudRepository<User, Integer> {

    User findByUsername(String username);

    void deleteByUsername(String username);

    List<User> findByBirthDate(Date date);

    void deleteById(int id);

    List<User> findTop5ByOrderBySignInCounterDesc();

}
