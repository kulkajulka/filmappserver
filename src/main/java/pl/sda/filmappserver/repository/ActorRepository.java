package pl.sda.filmappserver.repository;

import org.springframework.data.repository.CrudRepository;
import pl.sda.filmappmodel.model.Actor;

import java.util.List;

public interface ActorRepository extends CrudRepository<Actor, Integer> {

    Actor findById(int id);

    List<Actor> findByFirstNameOrLastName(String firstName, String lastName);

    List<Actor> deleteById(int id);

    Actor findByLastName(String lastName);

    public void deleteByLastName(String lastName);

}
