package pl.sda.filmappserver.repository;


import org.springframework.data.repository.CrudRepository;
import pl.sda.filmappmodel.model.Film;

import java.util.List;


public interface FilmRepository extends CrudRepository<Film, Integer> {

    Film findById(Integer id);

    List<Film> findByTitle(String title);

    List<Film> findByDirector(String director);

    List<Film> findByYear(Integer year);

    List<Film> findTop10ByOrderByImdbRatingDesc();
}
