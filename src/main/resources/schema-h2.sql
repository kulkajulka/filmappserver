CREATE SCHEMA IF NOT EXISTS FilmApp;
-- -----------------------------------------------------
-- Table FilmApp.actor
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS actor (
  actor_id INT(11) NOT NULL AUTO_INCREMENT,
  first_name VARCHAR(50) NOT NULL,
  last_name VARCHAR(50) NOT NULL,
  birth_date DATE NOT NULL,
  passing_date DATE NULL DEFAULT NULL,
  rate DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (actor_id));

-- -----------------------------------------------------
-- Table FilmApp.genre
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS genre (
  genre_id INT(11) NOT NULL,
  genre_type VARCHAR(30) NOT NULL,
  PRIMARY KEY (genre_id));

-- -----------------------------------------------------
-- Table FilmApp.film
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS film (
  film_id INT(11) NOT NULL AUTO_INCREMENT,
  title VARCHAR(150) NOT NULL,
  director CLOB NULL DEFAULT NULL,
  year int NOT NULL,
  imdb_rating DOUBLE NULL DEFAULT NULL,
  imdb_votes INT(11) NULL DEFAULT NULL,
  tomato_rating DOUBLE NULL DEFAULT NULL,
  tomato_reviews INT(11) NULL DEFAULT NULL,
  poster VARCHAR(255) NULL DEFAULT NULL,
  genre_id INT(11),
  PRIMARY KEY (film_id),
  CONSTRAINT fk_genre FOREIGN KEY (genre_id) REFERENCES genre (genre_id));

-- -----------------------------------------------------
-- Table FilmApp.user
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS user (
  user_id INT (11) NOT NULL AUTO_INCREMENT,
  user_name VARCHAR(30) NOT NULL,
  birth_date DATE NOT NULL,
  first_name VARCHAR(30) NULL DEFAULT NULL,
  last_name VARCHAR(30) NULL DEFAULT NULL,
  genre_preference VARCHAR(50) NULL DEFAULT NULL,
  sign_in_counter INT(11) NULL DEFAULT NULL,
  salt VARCHAR (50) NOT NULL,
  hash VARCHAR (100) NOT NULL,
  role ENUM('ADMIN', 'USER'),
  PRIMARY KEY (user_id));

-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS film_has_actor (
  film_actor_Id INT(11) NOT NULL AUTO_INCREMENT,
  film_id INT(11) NOT NULL,
  actor_id INT(11) NOT NULL,
  PRIMARY KEY (film_actor_Id),
  CONSTRAINT FK_film_id FOREIGN KEY (film_id) REFERENCES film (film_id),
  CONSTRAINT FK_actor_id FOREIGN KEY (actor_id) REFERENCES actor (actor_id));




