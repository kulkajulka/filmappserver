package pl.sda.filmappserver;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.filmappmodel.model.Actor;
import pl.sda.filmappserver.manager.ActorManager;


import java.util.Date;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CrudTestActor {


    @Autowired
    ActorManager actorManager;

    @Test
    @Transactional
    public void actorTests() {

        //Given
        Date date = new Date(System.currentTimeMillis() - 1000);
        Actor actor1 = new Actor("Piotr", "Nowak", date, date, 10.0);
        Actor actor2 = new Actor("Helka", "Patelka", date, date, 1);

        //Create & read
        //When
        actorManager.save(actor1);
        actorManager.save(actor2);
        //Then
        Assert.assertEquals(actorManager.findByLastName("Nowak"), actor1);
        Assert.assertNotEquals(actorManager.findByLastName("Patelka"), actor1);

        //Update
        //When
        Actor actorToUpdate = actorManager.findByLastName("Patelka");
        actorToUpdate.setFirstName("Bolek");
        actorManager.save(actorToUpdate);
        //Then
        Assert.assertEquals(actorManager.findByLastName("Patelka"), actor2);

        //Delete
        //When
        actorManager.deleteById(actorManager.findByFirstNameOrLastName("Bolek", "Patelka").get(0).getId());
        //Then
        Assert.assertTrue(actorManager.findByFirstNameOrLastName("Bolek", "Patelka").isEmpty());

        //When
        actorManager.deleteById(actorManager.findByFirstNameOrLastName("Piotr", "Nowak").get(0).getId());
        //Then
        Assert.assertTrue(actorManager.findByFirstNameOrLastName("Piotr", "Nowak").isEmpty());

    }

}
