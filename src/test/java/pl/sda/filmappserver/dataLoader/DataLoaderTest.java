package pl.sda.filmappserver.dataLoader;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.sda.filmappmodel.model.Actor;
import pl.sda.filmappmodel.model.Film;
import pl.sda.filmappmodel.model.Genre;
import pl.sda.filmappmodel.model.User;
import pl.sda.filmappserver.manager.ActorManager;
import pl.sda.filmappserver.manager.FilmManager;
import pl.sda.filmappserver.manager.GenreManager;
import pl.sda.filmappserver.manager.UserManager;

import java.util.List;

import static org.apache.log4j.Logger.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataLoaderTest {
    private static final Integer FILMS_COUNT = 2295;
    private static final Integer GENRES_COUNT = 28;
    private static final Logger LOGGER = getLogger(DataLoaderTest.class);

    @Autowired
    FilmManager filmManager;

    @Autowired
    ActorManager actorManager;

    @Autowired
    GenreManager genreManager;

    @Autowired
    UserManager userManager;

    @Test
    public void loadDataIntoH2AndAssertNotEmptyTables() throws Exception {
        List<Film> allFilms = filmManager.findAll();
        assertEquals(FILMS_COUNT, java.util.Optional.ofNullable(allFilms.size()).get());
        List<Actor> actors = actorManager.findAll();
        actors.forEach(LOGGER::info);
        List<Genre> genres = genreManager.findAll();
        assertEquals(GENRES_COUNT, java.util.Optional.ofNullable(genres.size()).get());
        genres.forEach(LOGGER::info);
        List<User> users = userManager.findAll();
        users.forEach(LOGGER::info);
    }

}