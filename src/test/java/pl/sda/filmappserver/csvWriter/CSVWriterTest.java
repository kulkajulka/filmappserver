package pl.sda.filmappserver.csvWriter;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.sda.filmappmodel.model.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

import static java.lang.System.currentTimeMillis;
import static org.apache.log4j.Logger.getLogger;
import static org.junit.Assert.assertNotNull;


public class CSVWriterTest {

    private final static Logger LOGGER = getLogger(CSVWriterTest.class);

    private Actor actor;
    private Film test;
    private Film test1;
    private Set<Actor> aktorsList;

    @Before
    public void initObject() {
        actor = new Actor(0,
                "Michau",
                "Nowak",
                new Date(currentTimeMillis()),
                new Date(currentTimeMillis() - 100),
                10.0);
        aktorsList = new HashSet<>();
        test = new Film(
                "Piraci z Karaibów",
                "Jan Kowalski",
                1990,
                new Genre(1, "western"),
                "test",
                aktorsList,
                new Tomato(10.0, 10),
                new Imdb(10.0, 10));
        aktorsList.add(actor);

        test1 = new Film(
                "Piła",
                "Michau Nowak",
                2015,
                new Genre(1, "western"),
                "5",
                aktorsList,
                new Tomato(10.0, 10),
                new Imdb(10.0, 10));
    }

    @After
    public void deleteDirectory() {
        try {
            FileUtils.deleteDirectory(Paths.get(System.getProperty("user.home"), "csv-films").toFile());
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }


    @Test
    public <T> void saveToCSVFile() throws Exception {
        //given
        List<T> lista = new ArrayList<>();
        lista.add((T) test);
        lista.add((T) test1);

        //when
        File file = CSVWriter.saveToCSVFile(lista);

        //then
        assertNotNull(file);
    }

    @Test
    public <T> void emptyObject() throws Exception {
        //given
        Film film = new Film();
        List<T> lista = new ArrayList<>();
        lista.add((T) film);

        //when
        File file = CSVWriter.saveToCSVFile(lista);

        //then
        assertNotNull(file);
    }

    @Test
    public <T> void listWithEmptyObject() throws Exception {
        //given
        Film film = new Film();
        List<T> lista = new ArrayList<>();
        lista.add((T) film);
        lista.add((T) test);
        lista.add((T) test1);

        //when
        File file = CSVWriter.saveToCSVFile(lista);

        //then
        assertNotNull(file);
    }

    @Test
    public <T> void listInList() throws Exception {
        //given
        List<List<T>> list = new ArrayList<>();
        List<T> lista = new ArrayList<>();
        lista.add((T) test);
        lista.add((T) test1);
        list.add(lista);

        //when
        File file = CSVWriter.saveToCSVFile(lista);

        //then
        assertNotNull(file);
    }
}