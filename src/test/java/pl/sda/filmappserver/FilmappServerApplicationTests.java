package pl.sda.filmappserver;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.sda.filmappmodel.model.Film;
import pl.sda.filmappmodel.model.User;
import pl.sda.filmappserver.manager.FilmManager;
import pl.sda.filmappserver.manager.UserManager;
import pl.sda.filmappserver.pdfreports.PdfGenerator;
import java.io.File;
import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FilmappServerApplicationTests {

	@Autowired
	PdfGenerator generator;

	@Autowired
    FilmManager filmManager;

	@Autowired
    UserManager userManager;

	@Test
	public void contextLoads() {
	}

	@Test
    public void generateFilmPdfInConfiguredCatalogue() {
        // given
        String fileName = "filmtest";
        List<Film> filmList = filmManager.findTopTenByImdbRating();
        String reportTitle = "filmtitle";
        // when
        generator.generate(fileName, filmList, reportTitle);
        // then
        assertTrue(new File(generator.getRESOURCES_PATH()+"filmtest.pdf").isFile());
    }

    @Test
    public void generateUserPdfInConfiguredCatalogue() {
	    // given
        String fileName = "usertest";
        List<User> userList = userManager.findTop5BySignIn();
        String usertitle = "usertitle";
        // when
        generator.generate(fileName, userList,usertitle);
        // then
        assertTrue(new File(generator.getRESOURCES_PATH()+"usertest.pdf").isFile());
    }
}
